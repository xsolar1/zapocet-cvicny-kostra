#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include "functions.h"

void cleanUpUsers();

struct user {
	char username[USERNAME_MAX];
	unsigned char salt[SALTLEN];
	unsigned char passhash[SHA256_DIGEST_LENGTH];
	unsigned long long balance;
};

struct transaction {
	struct user *sender;
	struct user *receiver;
	unsigned long long amount;
	struct transaction *next;
};

struct user_node{
	struct user *u;
	struct user_node *next;
};

struct user_node *uhead;
struct transaction *thead;

struct user* getUser(char username[USERNAME_MAX]){
	for(struct user_node *un = uhead; un; un = un->next){
		if (!strcmp(username, un->u->username))
			return un->u;
	}
	return NULL;
}

int addTransaction(){

	char username[USERNAME_MAX];
	char password[PASSWORD_MAX];
	
	printf("username: ");
	scanf("%s", username);
	printf("password: ");
	scanf("%s", password);
	
	// authenticate
	struct user* u = getUser(username);
	if (!u){
		fprintf(stderr, "Authentication failed: Unknown username.\n");
		return 2;
	}
	unsigned char md[SHA256_DIGEST_LENGTH];
	hash(password, strlen(password), u->salt, SALTLEN, md);
	if (memcmp(u->passhash, md, SHA256_DIGEST_LENGTH)){
		fprintf(stderr, "Authentication failed: Incorrect password.\n");
		return 3;
	}
	
	// get and validate the receiver
	char receiver[USERNAME_MAX];
	printf("receiver: ");
	scanf("%s", receiver);
	
	struct user* r = getUser(receiver);
	if (!r){
		fprintf(stderr, "This user does not exist. Aborting.\n");
		return 4;
	}

	// get and validate the transfered amount	
	unsigned long long amount;
	printf("amount: ");
	scanf("%llu", &amount);
	flushBuffer();
	
	unsigned long long totalTransfer = 0; 	 
	for(struct transaction *t = thead; t; t = t->next){
		if (t->sender == u)		
			totalTransfer += t->amount;
	}
	if (totalTransfer + amount > u->balance){
		fprintf(stderr, "You cannot transfer more money than you have.\n");
		return 5;
	}
	
	// actually create the transaction
	struct transaction *t = malloc(sizeof(struct transaction));
	if (!t)
		return -1;
	
	t->sender = u;
	t->receiver = r;
	t->amount = amount;
	
	// check if there is another transaction between these two accounts
	// and if so, merge them
	if (thead){
		for(struct transaction *transaction = thead; transaction->next; transaction = transaction->next){
			if (transaction->next->sender == t->sender && transaction->next->receiver == t->receiver){
				t->amount += transaction->next->amount;
				struct transaction *tmp = transaction->next->next;
				free(transaction->next);
				transaction->next = tmp;
			}
		}
		// check the very last transaction in the queue (the one thead points to)
		if (thead->sender == t->sender && thead->receiver == t->receiver){
				thead->amount += t->amount;
				free(t);
				return 0;
		}
	}
	
	// add the transaction to the queue
	t->next = thead;
	thead = t;
	
	return 0;
}

void printTransactions(){
	for(struct transaction *t = thead; t; t = t->next){
		printf("%-20s\t->\t%20s\t%llu\n", t->sender->username, t->receiver->username, t->amount);
	}
}

void printUsers(){
	for(struct user_node *un = uhead; un; un = un->next){
		printf("%-20s\t%llu\n", un->u->username, un->u->balance);
	}
}

#ifdef ASSEMBLY
int addUser(){

	struct user *u = malloc(sizeof(struct user));
	if (!u)
		return -1;
	struct user_node *un = malloc(sizeof(struct user_node));
	if (!un){
		free(u);
		return -2;
	}
	
	char password[PASSWORD_MAX];
	printf("username: ");
	scanf("%s", u->username);
	printf("password: ");
	scanf("%s", password);
	printf("balance: ");
	scanf("%llu", &(u->balance));
	flushBuffer();
	
	// get random salt
	RAND_bytes(u->salt, SALTLEN);
	
	// from password and salt compute attribute passhash
	unsigned char md[SHA256_DIGEST_LENGTH];
	hash(password, strlen(password), u->salt, SALTLEN, md);
	memcpy(u->passhash, md, SHA256_DIGEST_LENGTH);

	un->u = u;
	un->next = uhead;
	uhead = un;
	
	//printf("\nAdded user \'%s\' with password \'%s\' and balance %llu.\n", u->username, password, u->balance);
	
	return 0;
}


int writeUsers(){
	
	// get the output filename
	char filename[64];
	//printf("filename: ");
	scanf("%s", filename);
	
	FILE *file = fopen(filename, "wb");
	if (!file)
		return -1;
	
	for(struct user_node *un = uhead; un; un = un->next){
		fwrite(un->u, sizeof(struct user), 1, file);
	}
	
	return fclose(file);
}

#endif

int readUsers(char *filename){
	
	FILE *file = fopen(filename, "rb");
	if (!file){
		perror("Cannot open the file");
		return -1;
	}
	
	while (1) {
		struct user *u = malloc(sizeof(struct user));
		if (!u){
			cleanUpUsers();
			fclose(file);
			return -2;
		}
		
		if(!fread(u, sizeof(struct user), 1, file)){
			free(u);
			break;
		}

		struct user_node *un = malloc(sizeof(struct user_node));
		if (!un){
			free(u);
			cleanUpUsers();
			fclose(file);
			return -2;
		}
		un->u = u;
		un->next = uhead;
		uhead = un;
	}

	return fclose(file);	
}

void cleanUpUsers(){
	struct user_node *un = uhead; 
	while(un){
		free(un->u);
		struct user_node *tmp = un->next;
		free(un);
		un = tmp;
	}
	uhead = NULL;
}

void cleanUpTransactions(){
	struct transaction *t = thead; 
	while(t){
		struct transaction *tmp = t->next;
		free(t);
		t = tmp;
	}
	thead = NULL;
}


int main(int argc, char* argv[]){
	
	if (argc != 2){
		fprintf(stderr, "Usage: {program name} {file}\n");
		return -1;
	}
	
	int ret;
	if ((ret = readUsers(argv[1]))){
		fprintf(stderr, "There was a problem loading the user data.\n");
		return ret;
	}
	
	char buffer[64];

	printf("> ");
	while (fgets(buffer, 64, stdin)){
		
		size_t i;	
		for(i = strlen(buffer) - 1; isspace(buffer[i]); i--);
		buffer[i+1] = '\0';
			
		if (!strcmp(buffer, "print users")){
			printUsers();
		}
		#ifdef ASSEMBLY
		else if (!strcmp(buffer, "write users")){
			writeUsers();
		} else if (!strcmp(buffer, "add user")){
			addUser();
		} 
		#endif
		else if (!strcmp(buffer, "add transaction")){
			addTransaction();
		} else if (!strcmp(buffer, "print transactions")){
			printTransactions();
		} else if (!strcmp(buffer, "help")){
			help();
		} else if (!strcmp(buffer, "quit")){
			cleanUpUsers();
			cleanUpTransactions();
			return 0;
		} else {
			fprintf(stderr, "Unknown command \'%s\'.\n", buffer);
			printf("Type \'help\' to see all available commands.\n");
		}
		printf("> ");
			
	}
	
	return 1;
}
