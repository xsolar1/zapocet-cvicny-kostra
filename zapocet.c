// here go your library includes

#include "functions.h"

struct user {
	char username[USERNAME_MAX];
	unsigned char salt[SALTLEN];
	unsigned char passhash[SHA256_DIGEST_LENGTH]; // hash produced from concatenated password + salt (in this order)
	unsigned long long balance; // account balance
};

struct transaction {
	struct user *sender;
	struct user *receiver;
	unsigned long long amount;
	struct transaction *next;
};

int main(int argc, char* argv[]){
	//TODO
	return 0;
}