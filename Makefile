CC = gcc
CFLAGS = -std=c99 -lcrypto -lssl -Werror -Wimplicit -Wall -pedantic -g
ZAPOCET = zapocet.c functions.c
RESENI = reseni.c functions.c

all: zapocet

zapocet: $(ZAPOCET)
	$(CC) $(CFLAGS) $(ZAPOCET) -o zapocet

reseni: $(RESENI)
	$(CC) $(CFLAGS) $(RESENI) -o reseni

assembly: $(RESENI)
	$(CC) $(CFLAGS) -D ASSEMBLY $(RESENI) -o assembly
