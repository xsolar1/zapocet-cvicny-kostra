#include "functions.h"

int simpleSHA256(void* input, unsigned long length, unsigned char* md)
{
    SHA256_CTX context;
    if(!SHA256_Init(&context))
        return -1;

    if(!SHA256_Update(&context, (unsigned char*)input, length))
        return -2;

    if(!SHA256_Final(md, &context))
        return -3;

    return 0;
}

int hash(char *password, size_t passlen, unsigned char *salt, size_t saltlen, unsigned char md[SHA256_DIGEST_LENGTH]){
	char *passSalt = malloc(passlen + saltlen);
	if (!passSalt)
		return 1;
	memcpy(passSalt, password, passlen);
	memcpy(passSalt + passlen, salt, saltlen);
	
	int ret = simpleSHA256(passSalt, passlen + saltlen, md);
	free(passSalt);
	return ret;
}


void flushBuffer(){
	char c;
	while ((c = getchar()) != '\n' && c != EOF);
}

void help(){
	puts("The following commands are supported:");
	puts("add transaction\t adds a transaction to the queue");
	puts("print users\t prints all loaded usernames");
	puts("print transactions\t prints all transactions");
	puts("quit\t quits the program");
	puts("help\t prints this message");
}

