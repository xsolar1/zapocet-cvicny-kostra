#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// libraries to work with OpenSSL
#include <openssl/rand.h>
#include <openssl/evp.h>
#include <openssl/sha.h>

/**
  * Maximum number of characters a username can contain
  */
#define USERNAME_MAX 64

/** 
  * Maximum number of characters a user password can contain
  */
#define PASSWORD_MAX 64

/**
  * Number of characters each salt consists of
  */
#define SALTLEN	32

/** @brief Computes the SHA256 hash for a given password and salt (it concatenates them
  * in this order: password + salt)
  * @param password
  * @param passlen length of the password
  * @param salt
  * @param saltlen length of the salt (typically defined as SALTLEN)
  * @param md the output hash
  * @return 0 on success
  */
int hash(char *password, size_t passlen, unsigned char *salt, size_t saltlen, unsigned char md[SHA256_DIGEST_LENGTH]);

/** @brief Flushes the IO buffer for stdin 
  * It deletes all characters in buffer and stops at EOF or \n
  * (whichever happens first)
  */
void flushBuffer();

/** @brief prints a list of all available commands and their usage
  */
void help();





